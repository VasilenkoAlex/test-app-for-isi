(function() {
  'use strict';

  angular
    .module('carInfo')
    .component('carInfo', {
      bindings: {
        onSwitch: '&',
        onSave: '&',
        car: '<'
      },
      controller: carInfoController,
      templateUrl: 'components/car-info/car-info.template.html'

    });

  carInfoController.$inject = [];

  /* @ngInject */
  function carInfoController(dependencies) {

    var self = this;

    self.showMain = true;
    self.hidePane = hidePane;
    self.saveDetails = saveDetails;

    function hidePane() {
    
      self.onSwitch();

    }

    function saveDetails(car) {

      self.onSave({car: self.car});

    }
  }
})();