(function() {
  'use strict';

  angular
    .module('carTable')
    .component('carTable', {
      bindings: {
        onSwitch: '&',
        onEdit: '&',
        autos: '<'

      },
      controller: testAppController,
      templateUrl: 'components/car-table/car-table.template.html'

    });

  testAppController.$inject = [];

  /* @ngInject */
  function testAppController(dependencies) {
    var self = this;
    
    self.showInfo = showInfo;
    self.editCar = editCar;

    function showInfo() {
      self.onSwitch();
    }

    function editCar(car) {
      self.onEdit({carId: self.autos.indexOf(car)});
    }
  }
})();