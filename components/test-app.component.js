(function() {
  'use strict';

  angular
    .module('testApp')
    .component('testApp', {
      bindings: {

      },
      controller: testAppController,
      templateUrl: 'components/test-app.template.html'
    });

  testAppController.$inject = ['defaultAutos', 'ngLocalStorage'];

  /* @ngInject */
  function testAppController(defaultAutos, ngLocalStorage) {

    var self= this;

    self.showInfo = true;
    self.editExisting = false;
    self.switchInfo = switchInfo;
    self.saveInfo = saveInfo;
    self.editCar = editCar;

    if (ngLocalStorage.get('autos')) {

      self.autos = JSON.parse(ngLocalStorage.get('autos'));

    } else {

      self.autos = defaultAutos.autos;

    }

    function switchInfo() {

      self.showInfo = !self.showInfo;
      self.car = {};
      self.editExisting = false;

    }

    function saveInfo(car) {
      
      if (!self.editExisting) {

        self.autos.push(car); 

      } else {

        self.autos[self.carID] = self.car;

      }

      ngLocalStorage.set('autos', self.autos);
      self.switchInfo();
    }

    function editCar(carID) {

      self.carID = carID;
      self.showInfo = true;
      self.editExisting = true;
      self.car = JSON.parse(JSON.stringify(self.autos[carID]));

      ngLocalStorage.set('autos', self.autos);
    }
  }
})();