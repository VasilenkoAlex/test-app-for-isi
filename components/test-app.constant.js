(function() {
'use strict';

  angular
    .module('testApp')
    .constant('defaultAutos', 
    	{
    		autos: [
        {
          year: '2014',
          vehicleID: '2169',
          carName: 'Atego',
          model: '1823',
          type: 'Bus',
          comment: ''
        },
        {
          year: '2004',
          vehicleID: '3212',
          carName: 'Actros',
          model: '2542',
          type: 'Bus',
          comment: ''
        }
      ]});
})();