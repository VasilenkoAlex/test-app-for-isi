(function() {
  'use strict';

  angular
    .module('testApp')
    .service('ngLocalStorage', ngLocalStorage);

  ngLocalStorage.$inject = [];

  /* @ngInject */
  function ngLocalStorage() {
      
    return {

      get: function(data) {
        return localStorage.getItem(data);
      },
      
      set: function(name, data) {
        localStorage.setItem(name, JSON.stringify(data));
      }
    };
  }
})();