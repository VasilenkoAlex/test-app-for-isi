(function() {
'use strict';

  angular
    .module('testApp', [	
      'carTable',
      'carInfo'
    ]);
})();